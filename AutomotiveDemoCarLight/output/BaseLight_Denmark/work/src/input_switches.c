#include input_switches.h

void input_switches(int state) {

}
// get/set value for day_light 

#if FLAG_DRL_LOWBEAM || FLAG_DRL_LED || FLAG_DRL_BULB

static int day_light_value = 0;

void set_day_light(int state) {
	day_light_value = state;
}

int get_day_light() {
	return day_light.value;
}

#else

void set_day_light(int state) {
	log_error(ILLEGAL_DATAWRITE,"input_switches","day_light",state);
}

int get_day_light() {
	return 0;
}

#endif

// get/set value for fog_light 

#if FLAG_FOGLIGHT

static int fog_light_value = 0;

void set_fog_light(int state) {
	fog_light_value = state;
}

int get_fog_light() {
	return fog_light.value;
}

#else

void set_fog_light(int state) {
	log_error(ILLEGAL_DATAWRITE,"input_switches","fog_light",state);
}

int get_fog_light() {
	return 0;
}

#endif

// get/set value for light_detection 

#if FLAG_AUTOMATICLIGHT

static int light_detection_value = 0;

void set_light_detection(int state) {
	light_detection_value = state;
}

int get_light_detection() {
	return light_detection.value;
}

#else

void set_light_detection(int state) {
	log_error(ILLEGAL_DATAWRITE,"input_switches","light_detection",state);
}

int get_light_detection() {
	return 0;
}

#endif

// get/set value for lock_angle 

#if FLAG_ADAPTIVEFORWARDLIGHTING

static int lock_angle_value = 0;

void set_lock_angle(int state) {
	lock_angle_value = state;
}

int get_lock_angle() {
	return lock_angle.value;
}

#else

void set_lock_angle(int state) {
	log_error(ILLEGAL_DATAWRITE,"input_switches","lock_angle",state);
}

int get_lock_angle() {
	return 0;
}

#endif

// get/set value for light_mode 

static int light_mode_value;

void set_light_mode(int state) {
	light_mode_value = state;
}

int get_light_mode() {
	return light_mode.value;
}

// get/set value for high_beam 

static int high_beam_value;

void set_high_beam(int state) {
	high_beam_value = state;
}

int get_high_beam() {
	return high_beam.value;
}

// get/set value for brake_pedal 

static int brake_pedal_value;

void set_brake_pedal(int state) {
	brake_pedal_value = state;
}

int get_brake_pedal() {
	return brake_pedal.value;
}

// get/set value for reverse_movement 

static int reverse_movement_value;

void set_reverse_movement(int state) {
	reverse_movement_value = state;
}

int get_reverse_movement() {
	return reverse_movement.value;
}

// get/set value for turn_indicator 

static int turn_indicator_value;

void set_turn_indicator(int state) {
	turn_indicator_value = state;
}

int get_turn_indicator() {
	return turn_indicator.value;
}

//PV:IFCOND(pv:hasFeature('HazardWarning'))
// get/set value for warning_lights 

static int warning_lights_value;

void set_warning_lights(int state) {
	warning_lights_value = state;
}

int get_warning_lights() {
	return warning_lights.value;
}
//PV:ENDCOND
