// input_switches
void input_switches(int state);

void set_day_light(int state);
int get_day_light();

void set_fog_light(int state);
int get_fog_light();

void set_light_detection(int state);
int get_light_detection();

void set_lock_angle(int state);
int get_lock_angle();

void set_light_mode(int state);
int get_light_mode();

void set_high_beam(int state);
int get_high_beam();

void set_brake_pedal(int state);
int get_brake_pedal();

void set_reverse_movement(int state);
int get_reverse_movement();

void set_turn_indicator(int state);
int get_turn_indicator();

void set_warning_lights(int state);
int get_warning_lights();
